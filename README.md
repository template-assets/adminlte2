## Submodule Usage

1. Create a file `.gitmodules`
2. Run `git submodule add https://gitlab.com/template-assets/adminlte2.git <destination_folder>`
3. Commit your changes with submodule

### `.gitmodules` file example
```gitmodules
[submodule "public/assets"]
    path = public/assets
    url = https://gitlab.com/template-assets/adminlte2.git
```

## Pull a Git Submodule

In this section, we are going to see how you can pull a Git submodule as another developer on the project.

Whenever you are cloning a Git repository having submodules, you need to execute an extra command in order for the submodules to be pulled.

If you don’t execute this command, you will fetch the submodule folder, but you won’t have any content in it.

**To pull a Git submodule, use the “git submodule update” command with the “–init” and the “–recursive” options.**

```bash
git submodule update --init --recursive
```
## Update a Git Submodule

In some cases, you are not pulling a Git submodule but you are simply look to update your existing Git submodule in the project.

**In order to update an existing Git submodule, you need to execute the “git submodule update” with the “–remote” and the “–merge” option.**

```bash
git submodule update --remote --merge
```
Article Reference: [How To Add and Update Git Submodules](https://devconnected.com/how-to-add-and-update-git-submodules)
